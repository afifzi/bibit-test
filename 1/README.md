# Prerequisites
- Terraform v0.14.10 or later

# How-to
## Usage
```bash
$ export AWS_ACCESS_KEY_ID="anaccesskey"
$ export AWS_SECRET_ACCESS_KEY="asecretkey"
$ terraform init
$ terraform plan
$ terraform apply
```

## Clean Up
```bash
$ terraform destroy
```