# Create vpc
resource "aws_vpc" "this" {
  cidr_block = "10.0.0.0/16"

  enable_dns_support   = true
  enable_dns_hostnames = true
  
  tags = {
    Name = "test-vpc"
  }
}

# Create public subnet
resource "aws_subnet" "public" {
  vpc_id     = aws_vpc.this.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "public-subnet"
  }
}

# Create private subnet
resource "aws_subnet" "private" {
  vpc_id     = aws_vpc.this.id
  cidr_block = "10.0.2.0/24"

  tags = {
    Name = "private-subnet"
  }
}

# Create internet gateway
resource "aws_internet_gateway" "this" {
  vpc_id = aws_vpc.this.id

  tags = {
    Name = "test-igw"
  }
}

# Create elastic ip for nat gateway
resource "aws_eip" "this" {
  vpc = true

  tags = {
    Name = "natgw-eip"
  }
}

# Create nat gateway
resource "aws_nat_gateway" "this" {
  allocation_id = aws_eip.this.id
  subnet_id     = aws_subnet.private.id

  tags = {
    Name = "test-natgw"
  }

  depends_on = [
    aws_internet_gateway.this,
    aws_eip.this
  ]
}

# Create launch configuration for asg
resource "aws_launch_configuration" "this" {
  name          = "test-launch-configuration"
  image_id      = "ami-0e5182fad1edfaa68"
  instance_type = "t2.medium"
}

# Create asg
resource "aws_autoscaling_group" "this" {
  name                  = "test-asg"
  desired_capacity      = 2
  min_size              = 2
  max_size              = 5
  vpc_zone_identifier   = [aws_subnet.private.id]
  launch_configuration  = aws_launch_configuration.this.name
}

# Create autoscaling policy
resource "aws_autoscaling_policy" "this" {
  name                   = "autoscaling-policy"
  autoscaling_group_name = aws_autoscaling_group.this.name
  policy_type            = "TargetTrackingScaling"

  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }
    target_value = 45.0
  }
}