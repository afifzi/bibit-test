# How-to
## Usage
- Add DOCKER_CREDENTIALS environment variable with this value
```bash
$ echo -n USER:PASSWORD | base64
```
- Add SSH_PRIVATE_KEY environment variable with this value
```bash
$ cat ~/.ssh/id_rsa | base64
```
- Update Environment variable on .gitlab-ci.yml
```
DOCKER_REPOSITORY: afifzi
APP_NAME: nginx
APP_VERSION: 0.1.0
EC2_HOST: x.x.x.x
SSH_USER: afifzi
```
- Run pipeline